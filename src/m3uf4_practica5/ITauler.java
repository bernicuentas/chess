/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m3uf4_practica5;

/**
 *
 * @author Anonymous
 */
public interface ITauler {
    
    public void generarTauler();
    
    boolean moviment(Posicio origen, Posicio desti);
   
    void ubicarFigura(Figura f, Posicio posicio);
            
    String stringTauler();
}
