/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m3uf4_practica5.figures;

import m3uf4_practica5.Figura;
import m3uf4_practica5.Lletra;
import m3uf4_practica5.Posicio;

/**
 *
 * @author Anonymous
 */
public class Alfil extends Figura{
    
    
    public Alfil(Lletra lletra, String color, int valor) {
        super(lletra, color, valor);
    }
 

    @Override
    public boolean comprovarMoviment(Posicio origen, Posicio desti) {
        boolean moviment = false;
        int oX, dX, oY, dY, dif1, dif2;
        oX = origen.getX();
        dX = desti.getX();
        oY = origen.getY();
        dY = desti.getY();
        dif1 = Math.abs(oX - dX);
        dif2 = Math.abs(oY - dY);
        
        if (dif1 == dif2) {
            moviment = true;
        }

        return moviment;
    }
}
