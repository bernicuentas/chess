/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m3uf4_practica5.figures;

import m3uf4_practica5.Figura;
import m3uf4_practica5.Lletra;
import m3uf4_practica5.Posicio;
import m3uf4_practica5.Tauler;

/**
 *
 * @author Anonymous
 */
public class Torre extends Figura{
    Tauler tauler;
    public Torre(Lletra lletra, String color, int valor) {
        super(lletra, color, valor);
    }

    @Override
    public boolean comprovarMoviment(Posicio origen, Posicio desti) {
        boolean moviment = false;                  
        int oX, dX, oY, dY;
        oX = origen.getX();
        dX = desti.getX();
        oY = origen.getY();
        dY = desti.getY();
        
        if ((oX != dX && oY == dY) || (oX == dX && oY != dY)) {
            moviment = true;
        } 
        
        return moviment;
    }
}
