/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m3uf4_practica5;
/**
 *
 * @author Anonymous
 */
public abstract class Figura implements IColor, Comparable<Figura>{
    public Lletra lletra;
    public String color;
    public int valor;

    public Figura(Lletra lletra, String color, int valor) {
        this.lletra = lletra;
        this.color = color;
        this.valor = valor;
    }

    public Lletra getLletra() {
        return lletra;
    }

    public String getColor() {
        return color;
    }

    public int getValor() {
        return valor;
    }
    
    public abstract boolean comprovarMoviment(Posicio origen, Posicio desti);

    @Override
    public String toString() {
        return "Figura{" + "lletra=" + lletra + ", color=" + color + ", valor=" + valor + '}';
    }
   
    public int compareTo(Figura f) {
        return f.getValor() - valor;
    }
    
    
}
