/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m3uf4_practica5;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JFrame;
import m3uf4_practica5.figures.Alfil;
import m3uf4_practica5.figures.Cavall;
import m3uf4_practica5.figures.Dama;
import m3uf4_practica5.figures.Peo;
import m3uf4_practica5.figures.Rei;
import m3uf4_practica5.figures.Torre;

/**
 *
 * @author Anonymous
 */
public class Tauler extends JFrame implements ITauler, IColor {

    Casella[][] caselles = new Casella[8][8];

    public Tauler() {
    }

    public Casella[][] getCaselles() {
        return caselles;
    }

    public void iniciar() {
        generarTauler();
        System.out.println(stringTauler());
    }

    @Override
    public void generarTauler() {
        setLayout(new GridLayout(8, 8));

        //Inicialitzem la matriu
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                boolean esBlanca = ((i + j) % 2 == 0);

                Casella casella = new Casella(esBlanca ? Color.WHITE : Color.BLACK,
                        new Posicio(i, j), true);
                caselles[i][j] = casella;
            }
        }

        //Inicialitzem les caselles amb les figures pertinents blanques
        caselles[0][0] = new Casella(Color.BLACK, new Posicio(0, 0), (new Torre(Lletra.T, WHITE, 5)), false);
        caselles[1][0] = new Casella(Color.WHITE, new Posicio(1, 0), (new Cavall(Lletra.C, WHITE, 3)), false);
        caselles[2][0] = new Casella(Color.BLACK, new Posicio(2, 0), (new Alfil(Lletra.A, WHITE, 3)), false);
        caselles[3][0] = new Casella(Color.WHITE, new Posicio(3, 0), (new Rei(Lletra.R, WHITE, 0)), false);
        caselles[4][0] = new Casella(Color.BLACK, new Posicio(4, 0), (new Dama(Lletra.D, WHITE, 9)), false);
        caselles[5][0] = new Casella(Color.WHITE, new Posicio(5, 0), (new Alfil(Lletra.A, WHITE, 3)), false);
        caselles[6][0] = new Casella(Color.BLACK, new Posicio(6, 0), (new Cavall(Lletra.C, WHITE, 3)), false);
        caselles[7][0] = new Casella(Color.WHITE, new Posicio(7, 0), (new Torre(Lletra.T, WHITE, 5)), false);
        caselles[0][1] = new Casella(Color.WHITE, new Posicio(0, 1), (new Peo(Lletra.P, WHITE, 1)), false);
        caselles[1][1] = new Casella(Color.BLACK, new Posicio(1, 1), (new Peo(Lletra.P, WHITE, 1)), false);
        caselles[2][1] = new Casella(Color.WHITE, new Posicio(2, 1), (new Peo(Lletra.P, WHITE, 1)), false);
        caselles[3][1] = new Casella(Color.BLACK, new Posicio(3, 1), (new Peo(Lletra.P, WHITE, 1)), false);
        caselles[4][1] = new Casella(Color.WHITE, new Posicio(4, 1), (new Peo(Lletra.P, WHITE, 1)), false);
        caselles[5][1] = new Casella(Color.BLACK, new Posicio(5, 1), (new Peo(Lletra.P, WHITE, 1)), false);
        caselles[6][1] = new Casella(Color.WHITE, new Posicio(6, 1), (new Peo(Lletra.P, WHITE, 1)), false);
        caselles[7][1] = new Casella(Color.BLACK, new Posicio(7, 1), (new Peo(Lletra.P, WHITE, 1)), false);

        //Iniciatlitzem les figures pertinents negres
        caselles[0][7] = new Casella(Color.WHITE, new Posicio(0, 7), (new Torre(Lletra.T, BLACK, 5)), false);
        caselles[1][7] = new Casella(Color.BLACK, new Posicio(1, 7), (new Cavall(Lletra.C, BLACK, 3)), false);
        caselles[2][7] = new Casella(Color.WHITE, new Posicio(2, 7), (new Alfil(Lletra.A, BLACK, 3)), false);
        caselles[3][7] = new Casella(Color.BLACK, new Posicio(3, 7), (new Rei(Lletra.R, BLACK, 0)), false);
        caselles[4][7] = new Casella(Color.WHITE, new Posicio(4, 7), (new Dama(Lletra.D, BLACK, 9)), false);
        caselles[5][7] = new Casella(Color.BLACK, new Posicio(5, 7), (new Alfil(Lletra.A, BLACK, 3)), false);
        caselles[6][7] = new Casella(Color.WHITE, new Posicio(6, 7), (new Cavall(Lletra.C, BLACK, 3)), false);
        caselles[7][7] = new Casella(Color.BLACK, new Posicio(7, 7), (new Torre(Lletra.T, BLACK, 5)), false);
        caselles[0][6] = new Casella(Color.BLACK, new Posicio(0, 6), (new Peo(Lletra.P, BLACK, 1)), false);
        caselles[1][6] = new Casella(Color.WHITE, new Posicio(1, 6), (new Peo(Lletra.P, BLACK, 1)), false);
        caselles[2][6] = new Casella(Color.BLACK, new Posicio(2, 6), (new Peo(Lletra.P, BLACK, 1)), false);
        caselles[3][6] = new Casella(Color.WHITE, new Posicio(3, 6), (new Peo(Lletra.P, BLACK, 1)), false);
        caselles[4][6] = new Casella(Color.BLACK, new Posicio(4, 6), (new Peo(Lletra.P, BLACK, 1)), false);
        caselles[5][6] = new Casella(Color.WHITE, new Posicio(5, 6), (new Peo(Lletra.P, BLACK, 1)), false);
        caselles[6][6] = new Casella(Color.BLACK, new Posicio(6, 6), (new Peo(Lletra.P, BLACK, 1)), false);
        caselles[7][6] = new Casella(Color.WHITE, new Posicio(7, 6), (new Peo(Lletra.P, BLACK, 1)), false);

    }

    @Override
    public boolean moviment(Posicio origen, Posicio desti) {
        boolean moviment = false;
        Figura forigen, fdesti;
        int oX, dX, oY, dY, dif1, dif2;
        oX = origen.getX();
        dX = desti.getX();
        oY = origen.getY();
        dY = desti.getY();
        dif1 = Math.abs(oX - dX);
        dif2 = Math.abs(oY - dY);

        if (origen != desti) {
            forigen = caselles[origen.getX()][origen.getY()].getFigura();
            fdesti = caselles[desti.getX()][desti.getY()].getFigura();

            if (forigen.getLletra().equals(Lletra.P)
                    && caselles[desti.getX()][desti.getY()].isBuida()) {
                System.out.println(desti);
                if (forigen.comprovarMoviment(origen, desti)) {
                    ubicarFigura(forigen, desti);
                    cambiarBooleanBuidaOrigen(origen);
                    cambiarBooleanBuidaDesti(desti);
                    moviment = true;
                }

            } else {
                if (caselles[desti.getX()][desti.getY()].isBuida()) {
                    if (forigen.comprovarMoviment(origen, desti)) {
                        ubicarFigura(forigen, desti);
                        cambiarBooleanBuidaOrigen(origen);
                        cambiarBooleanBuidaDesti(desti);
                        moviment = true;
                    }
                } else if (!(forigen.getColor().equals(fdesti.getColor()))) {
                    if (forigen.comprovarMoviment(origen, desti)) {
                        ubicarFigura(forigen, desti);
                        cambiarBooleanBuidaOrigen(origen);
                        cambiarBooleanBuidaDesti(desti);
                        moviment = true;
                    } else {
                        if (dif1 == dif2) {
                            System.out.println(desti);
                            ubicarFigura(forigen, desti);
                            cambiarBooleanBuidaOrigen(origen);
                            cambiarBooleanBuidaDesti(desti);
                            moviment = true;
                        }
                    }
                }
            }
        }
        System.out.println(stringTauler());
        return moviment;
    }

    public void cambiarBooleanBuidaOrigen(Posicio posicio) {
        caselles[posicio.getX()][posicio.getY()].setBuida(true);
    }

    public void cambiarBooleanBuidaDesti(Posicio posicio) {
        caselles[posicio.getX()][posicio.getY()].setBuida(false);
    }

    @Override
    public void ubicarFigura(Figura f, Posicio posicio) {
        caselles[posicio.getX()][posicio.getY()].setFigura(f);
    }

    @Override
    public String stringTauler() {
        StringBuilder cad = new StringBuilder();
        boolean esBlanca, colorLletra;
        cad.append("\t\t-------------------\n");
        cad.append("");
        for (int y = 0; y < 8; y++) {
            cad.append("\t\t" + (y + 1)).append("|");
            for (int x = 0; x < 8; x++) {
                esBlanca = ((x + y) % 2 == 0);
                if (caselles[x][y].isBuida()) {
                    cad.append(esBlanca ? PURPLE_BACKGROUND : WHITE_BACKGROUND);
                    cad.append("  ");
                } else {
                    if (caselles[x][y].getFigura().getColor().equals(WHITE)) {
                        colorLletra = true;
                    } else {
                        colorLletra = false;
                    }
                    String lletras = "";
                    lletras = caselles[x][y].getFigura().getLletra() + " ";
                    cad.append(esBlanca ? PURPLE_BACKGROUND : WHITE_BACKGROUND);
                    cad.append(colorLletra ? BLUE : GREEN);
                    cad.append(lletras);
                }
            }
            cad.append(ANSI_RESET); //Estableix la configuració per defecte
            cad.append("|\n");
        }
        cad.append("\t\t-------------------\n");
        cad.append("\t\t  a b c d e f g h\n");
        return cad.toString();
    }

    public class Casella {

        Color color;
        Posicio posicio;
        Figura figura;
        boolean buida;

        public Casella(Color color, Posicio posicio, boolean buida) {
            this.color = color;
            this.posicio = posicio;
            this.buida = buida;
        }

        public Casella(Color color, Posicio posicio, Figura figura, boolean buida) {
            this.color = color;
            this.posicio = posicio;
            this.figura = figura;
            this.buida = buida;
        }

        public Color getColor() {
            return color;
        }

        public Posicio getPosicio() {
            return posicio;
        }

        public Figura getFigura() {
            return figura;
        }

        public void setFigura(Figura figura) {
            this.figura = figura;
        }

        public boolean isBuida() {
            return buida;
        }

        public void setBuida(boolean buida) {
            this.buida = buida;
        }

        @Override
        public String toString() {
            return "Casella{" + "color=" + color + ", posicio=" + posicio + ", figura=" + figura + '}';
        }

    }
}
