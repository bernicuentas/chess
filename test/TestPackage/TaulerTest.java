/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestPackage;

import m3uf4_practica5.Lletra;
import m3uf4_practica5.Posicio;
import m3uf4_practica5.Tauler;
import m3uf4_practica5.figures.Dama;

/**
 *
 * @author Anonymous
 */
public class TaulerTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Tauler t = new Tauler();
        Dama d = new Dama(Lletra.D, "\u001B[37m", 9);

        t.iniciar();
        Posicio p1 = new Posicio(0, 1);
        Posicio p2 = new Posicio(0, 3);
        
        Posicio p3 = new Posicio(0, 6);
        Posicio p4 = new Posicio(0, 4);
        
        Posicio p5 = new Posicio(1, 1);
        Posicio p6 = new Posicio(1, 3);
        
        Posicio p7 = new Posicio(0, 4);
        Posicio p8 = new Posicio(1, 3);
        
        Posicio d1 = new Posicio (4,4);

        t.moviment(p1, p2);
        t.moviment(p3, p4);
        t.moviment(p5, p6);
        t.moviment(p7, p8);
        
        t.ubicarFigura(d, d1);
        t.cambiarBooleanBuidaDesti(d1);
        System.out.println(t.stringTauler());
    }

}
